package com.incubateur.merceco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MercecoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MercecoApplication.class, args);
	}

}
