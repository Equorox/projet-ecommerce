package com.incubateur.merceco.repositories;

import com.incubateur.merceco.entities.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    Optional<Article> findByName(String name);

}
