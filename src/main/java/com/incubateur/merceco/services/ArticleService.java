package com.incubateur.merceco.services;

import com.incubateur.merceco.dto.ArticleDto;

import java.util.List;

public interface ArticleService {

    ArticleDto getById(long id);

    List<ArticleDto> getAllArticles();

    ArticleDto getArticleByName(String name);

    ArticleDto saveOrUpdate(ArticleDto article);
}